package br.ucsal.bes.poo20191.lista01.cristianofilho;

import java.util.Scanner;

/*
 *@author Cristiano Filho [cristiano.filho@ucsal.edu.br]
 * @version 1.0
 */

public class Questao02 {

	public static void main(String[] args) {
		// variaveis
		int primeironum;
		int segundonum;
		Scanner sc = new Scanner(System.in);
		// processamento de dados
		System.out.println("Informe o primeiro numero: ");
		primeironum = sc.nextInt();
		System.out.println("Informe o segundo numero: ");
		segundonum = sc.nextInt();
		// saida
		System.out.println(primeironum + segundonum);

	}

}
