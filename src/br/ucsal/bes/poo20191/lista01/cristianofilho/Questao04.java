package br.ucsal.bes.poo20191.lista01.cristianofilho;

import java.util.Scanner;

public class Questao04 {

	public static void main(String[] args) {
		System.out.println("introduza a letra:");
		Scanner sc = new Scanner(System.in);
		String LETRA = sc.next();
		char x = LETRA.charAt(0);
		System.out.println(LETRA);
		switch (x) {
		case 'a':
		case 'e':
		case 'i':
		case 'o':
		case 'u':
			System.out.println("a letra " + LETRA + " � uma vogal");
			break;
		default:
			System.out.println("a letra " + LETRA + " � uma consoante");
		}
	}
}
